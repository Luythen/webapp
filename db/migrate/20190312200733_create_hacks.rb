class CreateHacks < ActiveRecord::Migration[5.2]
  def change
    create_table :hacks do |t|
      t.string :name
      t.text :description
      t.string :download_url
      t.integer :difficulty
      t.integer :total_stars
      t.integer :required_stars
      t.string :creator

      t.timestamps
    end
  end
end
