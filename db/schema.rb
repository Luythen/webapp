# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_16_193749) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "data_requests", force: :cascade do |t|
    t.integer "record_type"
    t.integer "record_id"
    t.bigint "user_id"
    t.text "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_data_requests_on_user_id"
  end

  create_table "downloads", force: :cascade do |t|
    t.string "version"
    t.string "comment"
    t.string "contributors"
    t.string "everdrive"
    t.string "release_date"
    t.bigint "hack_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "download_url"
    t.index ["hack_id"], name: "index_downloads_on_hack_id"
  end

  create_table "hacks", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "download_url"
    t.string "difficulty"
    t.string "total_stars"
    t.string "required_stars"
    t.string "creator"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "youtube_url"
    t.string "fandom_link"
  end

  create_table "reports", force: :cascade do |t|
    t.bigint "hack_id"
    t.text "reason"
    t.string "ipaddress"
    t.boolean "read"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hack_id"], name: "index_reports_on_hack_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.boolean "admin", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "data_requests", "users"
  add_foreign_key "downloads", "hacks"
  add_foreign_key "reports", "hacks"
end
