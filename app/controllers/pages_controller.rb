class PagesController < ApplicationController
  def index
    @page_title = "Home"
  end

  def help
    @page_title = "Help"
  end

  def error
    @page_title = "Error"
  end

  def notimplemented
    @page_title = "Not implemented"
  end

  def test
    @page_title = "Test Page"
  end
end
